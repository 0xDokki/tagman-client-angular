import {Component, ElementRef, ViewChild} from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {UserApiService} from '../user-api/user-api.service';
import {AlertService} from '../alerts/alert.service';
import {TagDialogComponent} from '../tag-dialog/tag-dialog.component';
import {Tag} from '../tag-api/tag-api.service';

/*
 * This component has no tests of its own, since it can't exist outside
 * of the modal on the app.component page.
 */

@Component({
    selector: 'app-user-settings',
    templateUrl: './user-settings.component.html',
    styleUrls: ['./user-settings.component.sass'],
})
export class UserSettingsComponent {
    private blacklistTags: string[] = [];
    private selectedTags: string[] = [];

    constructor(private activeModal: NgbActiveModal,
                private modalService: NgbModal,
                private userService: UserApiService,
                private alertService: AlertService) {

        userService.getBlacklist().then((blacklist: string[]) => this.blacklistTags = blacklist);
    }

    logoutWrapper() {
        this.userService.logout()
            .then(() => this.alertService.pushAlert({type: 'success', message: 'Successfully logged out'}, 5000));
        this.activeModal.dismiss();
    }

    openTagSelect() {
        const ref = this.modalService.open(TagDialogComponent, {size: 'lg'});
        ref.componentInstance.excludeFromView = this.blacklistTags;
        ref.componentInstance.okText = 'Blacklist';
        ref.result.then((tags: string[]) => {
            this.userService.addToBlacklist(tags)
                .then(() => this.userService.getBlacklist().then((blacklist: string[]) => this.blacklistTags = blacklist))
                .catch(err => this.alertService.pushAlert({type: 'danger', message: err.error.message}, 5000));
        }).catch(() => {
            // no tags have been selected, but we need to catch the promise
        });
    }

    deleteSelectedFromBlacklist() {
        this.userService.removeFromBlacklist(this.selectedTags)
            .then(() => this.selectedTags = [])
            .then(() => this.userService.getBlacklist().then((blacklist: string[]) => this.blacklistTags = blacklist))
            .catch(err => this.alertService.pushAlert({type: 'danger', message: err.error.message}, 5000));
    }

    selectTag(tag: string) {
        if (this.selectedTags.indexOf(tag) >= 0) this.selectedTags.splice(this.selectedTags.indexOf(tag), 1);
        else this.selectedTags.push(tag);
    }
}
