import {Component, ElementRef, ViewChild} from '@angular/core';
import {NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {UserApiService} from './user-api/user-api.service';
import {UserSettingsComponent} from './user-settings/user-settings.component';
import {AlertService} from './alerts/alert.service';
import {TagDialogComponent} from './tag-dialog/tag-dialog.component';
import {GroupDialogComponent} from './group-dialog/group-dialog.component';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.sass'],
})
export class AppComponent {
    navbarCollapsed = true;

    searchQuery: string;
    loginStage: 'Username' | 'Password' = 'Username';
    loginMode: 'login' | 'register' = 'login';

    @ViewChild('login_modal', {static: true}) loginModalTemplate: ElementRef;
    loginModal: NgbModalRef;

    loginUsername: string;
    loginPassword: string;

    constructor(private modalService: NgbModal,
                private userService: UserApiService,
                private alertService: AlertService) {
        // todo: hide login button when user accounts are disabled
        userService.updateStatus();
    }

    openLoginModal() {
        this.loginModal = this.modalService.open(this.loginModalTemplate, {ariaLabelledBy: 'modal-basic-title'});
    }

    advanceLoginModal() {
        if (this.loginStage === 'Username') {
            // show password stage next
            this.loginStage = 'Password';

            this.userService.getUser(this.loginUsername).then((user) => {
                this.loginMode = !!user ? 'login' : 'register';
            });
        } else {
            const clearInput = () => {
                this.loginStage = 'Username';
                this.loginUsername = '';
                this.loginPassword = '';
            };

            if (this.loginMode === 'register') {
                this.userService.registerNew(this.loginUsername, this.loginPassword)
                    .then(() => this.userService.login(this.loginUsername, this.loginPassword))
                    .then(() => this.alertService.pushAlert({type: 'success', message: 'Successfully logged in'}, 5000))
                    .catch(err => this.alertService.pushAlert({type: 'warning', message: err.error.message}, 5000))
                    .finally(clearInput);
            } else {
                this.userService.login(this.loginUsername, this.loginPassword)
                    .then(() => this.alertService.pushAlert({type: 'success', message: 'Successfully logged in'}, 5000))
                    .catch(err => this.alertService.pushAlert({type: 'warning', message: err.error.message}, 5000))
                    .finally(clearInput);
            }
            this.loginModal.close(true);
        }
    }

    openUserSettingsModal() {
        this.modalService.open(UserSettingsComponent);
    }

    openTagEditor() {
        const ref = this.modalService.open(TagDialogComponent, {size: 'lg'});
        ref.componentInstance.allowCreation = true;
        ref.componentInstance.allowGroupEditing = true;
    }

    openGroupEditor() {
        const ref = this.modalService.open(GroupDialogComponent, {size: 'lg'});
        ref.componentInstance.okText = 'Done';
        ref.componentInstance.allowTagAssigning = true;
    }
}
