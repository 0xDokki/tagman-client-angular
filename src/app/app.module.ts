import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HttpClientModule} from '@angular/common/http';
import {UserSettingsComponent} from './user-settings/user-settings.component';
import {TagDialogComponent} from './tag-dialog/tag-dialog.component';
import {GroupDialogComponent} from './group-dialog/group-dialog.component';
import { DetailViewComponent } from './detail-view/detail-view.component';
import { DateFormatterPipe } from './date-formatter/date-formatter.pipe';

@NgModule({
    declarations: [
        AppComponent,
        UserSettingsComponent,
        TagDialogComponent,
        GroupDialogComponent,
        DetailViewComponent,
        DateFormatterPipe,
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        AppRoutingModule,
        FormsModule,
        NgbModule,
    ],
    bootstrap: [AppComponent],
    entryComponents: [UserSettingsComponent, TagDialogComponent, GroupDialogComponent],
})
export class AppModule {
}
