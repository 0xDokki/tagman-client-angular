import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import SHA3 from 'sha3';

const API_URL = environment.backendUrl;

@Injectable({
    providedIn: 'root',
})
export class UserApiService {
    public CurrentUser: User;

    constructor(private http: HttpClient) {
    }

    getUser(username: string) {
        return this.http.get<User>(`${API_URL}/user/${username}`).toPromise();
    }

    registerNew(username: string, password: string) {
        const hash = new SHA3(512);
        hash.update(password);

        return this.http.post(`${API_URL}/user`, {
            username, password: hash.digest('base64'),
        }).toPromise();
        // todo: decide if error handling should be centralized or left to the service consumer
    }

    login(username: string, password: string) {
        const hash = new SHA3(512);
        hash.update(password);

        return this.http.post(`${API_URL}/user/auth/${username}`, {
            password: hash.digest('base64'),
        }, {withCredentials: true}).toPromise().then(this.updateStatus);
    }

    logout() {
        return this.http.delete(`${API_URL}/user/auth`, {withCredentials: true}).toPromise().then(this.updateStatus);
    }

    updateStatus = () => {
        return this.http.get<User>(`${API_URL}/user/auth`, {withCredentials: true}).toPromise().then(usr => this.CurrentUser = usr);
    };

    getBlacklist() {
        return this.http.get<string[]>(`${API_URL}/user/blacklist`, {withCredentials: true}).toPromise();
    }

    addToBlacklist(tag: string[]) {
        const promises = [];
        for (const key of tag) {
            promises.push(this.http.put(`${API_URL}/user/blacklist/${key}`, {}, {withCredentials: true}).toPromise());
        }
        return Promise.all(promises);
    }

    removeFromBlacklist(tag: string[]) {
        const promises = [];
        for (const key of tag) {
            promises.push(this.http.delete(`${API_URL}/user/blacklist/${key}`, {withCredentials: true}).toPromise());
        }
        return Promise.all(promises);
    }

}

export interface User {
    username: string;
    profile_picture: string;
}
